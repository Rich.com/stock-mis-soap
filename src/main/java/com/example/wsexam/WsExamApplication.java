package com.example.wsexam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WsExamApplication {

	public static void main(String[] args) {
		SpringApplication.run(WsExamApplication.class, args);
	}

}
