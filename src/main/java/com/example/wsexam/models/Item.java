package com.example.wsexam.models;

import com.example.wsexam.enums.EItemStatus;
import com.sun.istack.NotNull;

import javax.persistence.*;

@Entity(name = "items")
public class Item {
    @Id
    @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String itemCode;

    @Enumerated(EnumType.STRING)
    @NotNull
    private EItemStatus status;

    private Long price;

    private Long supplier;

    public Item(){}

    public Item(Long id, String name, String itemCode, EItemStatus status, Long price, Long supplier){
        this.id = id;
        this.name = name;
        this.itemCode = itemCode;
        this.status = status;
        this.price = price;
        this.supplier = supplier;
    }

    public Item(String name, String itemCode, EItemStatus status, Long price, Long supplier){
        this.name = name;
        this.itemCode = itemCode;
        this.status = status;
        this.price = price;
        this.supplier = supplier;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getItemCode() {
        return itemCode;
    }

    public void setItemCode(String itemCode) {
        this.itemCode = itemCode;
    }

    public EItemStatus getStatus() {
        return status;
    }

    public void setStatus(EItemStatus status) {
        this.status = status;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Long getSupplier() {
        return supplier;
    }

    public void setSupplier(Long supplier) {
        this.supplier = supplier;
    }
}
