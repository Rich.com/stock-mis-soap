package com.example.wsexam.endpoints;

import com.example.wsexam.enums.EItemStatus;
import com.example.wsexam.repositories.IItemRepository;
import mike.stock_mis_soap.item.*;

import mike.stock_mis_soap.item.NewItemRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import com.example.wsexam.models.Item;

import java.util.List;
import java.util.Optional;

@Endpoint
public class ItemEndpoint {
    private final IItemRepository itemRepository;

    @Autowired
    public ItemEndpoint(IItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    @PayloadRoot(namespace = "mike/stock-mis-soap/item", localPart = "NewItemRequest")
    @ResponsePayload
    public NewItemRequest create(@RequestPayload NewItemRequest dto) {
        mike.stock_mis_soap.item.Item itemDTO = dto.getItem();
        Item _item = new Item(itemDTO.getName(), itemDTO.getItemCode(),
                itemDTO.getStatus(), itemDTO.getPrice(), itemDTO.getSupplier());
        Item item = itemRepository.save(_item);
        NewItemRequest itemResponse = new NewItemRequest();
        itemDTO.setId(item.getId());
        itemResponse.setItem(itemDTO);
        return itemResponse;
    }

    @PayloadRoot(namespace = "mike/stock-mis-soap/item", localPart = "GetAllItemsRequest")
    @ResponsePayload
    public GetAllItemsResponse findAll(@RequestPayload GetAllItemsRequest request){

        List<Item> items = itemRepository.findAll();

        GetAllItemsResponse response = new GetAllItemsResponse();

        for (Item item: items){
            mike.stock_mis_soap.item.Item _item = new mike.stock_mis_soap.item.Item();
            _item.setId(item.getId());
            _item.setName(item.getName());
            _item.setItemCode(item.getItemCode());
            _item.setStatus(item.getStatus());
            _item.setPrice(item.getPrice());
            _item.setSupplier(item.getSupplier());

            response.getItem().add(_item);
        }

        return response;
    }

    @PayloadRoot(namespace = "mike/stock-mis-soap/item", localPart = "GetItemByIdRequest")
    @ResponsePayload
    public GetItemByIdResponse findById(@RequestPayload GetItemByIdRequest request){
        Optional<Item> _item = itemRepository.findById(request.getId());

        if(!_item.isPresent())
            return new GetItemByIdResponse();

        Item item = _item.get();

        GetItemByIdResponse response = new GetItemByIdResponse();

        mike.stock_mis_soap.item.Item __item = new mike.stock_mis_soap.item.Item();
        __item.setId(item.getId());
        __item.setName(item.getName());
        __item.setItemCode(item.getItemCode());
        __item.setStatus(item.getStatus());
        __item.setPrice(item.getPrice());
        __item.setSupplier(item.getSupplier());


        response.setItem(__item);

        return response;
    }

    @PayloadRoot(namespace = "mike/stock-mis-soap/item", localPart = "DeleteItemRequest")
    @ResponsePayload
    public DeleteItemResponse delete(@RequestPayload DeleteItemRequest request){
        itemRepository.deleteById(request.getId());
        DeleteItemResponse response = new DeleteItemResponse();
        response.setMessage("Successfully deleted");
        return response;
    }

    @PayloadRoot(namespace = "mike/stock-mis-soap/item", localPart = "UpdateItemRequest")
    @ResponsePayload
    public UpdateItemResponse update(@RequestPayload UpdateItemRequest request){
        mike.stock_mis_soap.item.Item __item = request.getItem();

        Item _item = new Item(__item.getName(), __item.getItemCode(), __item.getStatus(),
                __item.getPrice(), __item.getSupplier());
        _item.setId(__item.getId());

        Item item = itemRepository.save(_item);

        UpdateItemResponse itemDTO = new UpdateItemResponse();

        __item.setId(item.getId());

        itemDTO.setItem(__item);

        return itemDTO;
    }
}
